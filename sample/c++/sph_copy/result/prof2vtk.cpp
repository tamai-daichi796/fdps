#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define IN_DIR "./"
#define OUT_DIR "./"


FILE* fp;
char filename[256];
int NumberOfParticle;
double Time;
int FileNumber = 2500;

double *Position;
double *Velocity;
double *Pressure;
double *eng;
double *dens;
int *ParticleType;

void read_data(char* in_dir, int iFile) {
	sprintf(filename, "%s/%04d.txt",in_dir,iFile);
	fp = fopen(filename, "r");
    fscanf(fp,"%lf",&Time);			printf("NumberOfParticle: %lf\n",Time);
	fscanf(fp,"%d",&NumberOfParticle);			printf("NumberOfParticle: %d\n",NumberOfParticle);

	Position = (double*)malloc(sizeof(double)*NumberOfParticle*3);
	Velocity = (double*)malloc(sizeof(double)*NumberOfParticle*3);
	Pressure = (double*)malloc(sizeof(double)*NumberOfParticle);
	eng = (double*)malloc(sizeof(double)*NumberOfParticle);
    dens = (double*)malloc(sizeof(double)*NumberOfParticle);
	ParticleType = (int*)malloc(sizeof(int)*NumberOfParticle);


	for(int i=0;i<NumberOfParticle;i++) {
		int a[1];
		double b[10];
		//fscanf(fp," %d %d %lf %lf %lf %lf %lf %lf %lf %lf",&a[0],&a[1],&b[0],&b[1],&b[2],&b[3],&b[4],&b[5],&b[6],&b[7]);
        fscanf(fp,"%d\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",&a[0],&b[0],&b[1],&b[2],&b[3],&b[4],&b[5],&b[6],&b[7],&b[8],&b[9]);
		ParticleType[i]=a[0];

		Position[i*3]=b[1];	Position[i*3+1]=b[2];	Position[i*3+2]=b[3];
		Velocity[i*3]=b[4];	Velocity[i*3+1]=b[5];	Velocity[i*3+2]=b[6];
        dens[i]=b[7];
        eng[i]=b[8];
		Pressure[i]=b[9];
	}
	fclose(fp);
}

void mk_vtu(char* out_dir, int iFile) {
	sprintf(filename, "%s/particle_%04d.vtu",out_dir,iFile);
    printf("Creating %s ... ", filename);

	FILE *fp;
	fp=fopen(filename,"w");
	fprintf(fp,"<?xml version='1.0' encoding='UTF-8'?>\n");
	fprintf(fp,"<VTKFile xmlns='VTK' byte_order='LittleEndian' version='0.1' type='UnstructuredGrid'>\n");
	fprintf(fp,"<UnstructuredGrid>\n");
 	fprintf(fp,"<Piece NumberOfCells='%d' NumberOfPoints='%d'>\n",NumberOfParticle,NumberOfParticle);

 	fprintf(fp,"<Points>\n");
 	fprintf(fp,"<DataArray NumberOfComponents='3' type='Float32' Name='Position' format='ascii'>\n");
	for(int i=0;i<NumberOfParticle;i++)fprintf(fp,"%lf %lf %lf\n",Position[i*3],Position[i*3+1],Position[i*3+2]);
 	fprintf(fp,"</DataArray>\n");
  	fprintf(fp,"</Points>\n");

  	fprintf(fp,"<PointData>\n");
 	fprintf(fp,"<DataArray NumberOfComponents='1' type='Int32' Name='ParticleType' format='ascii'>\n");
	for(int i=0;i<NumberOfParticle;i++){fprintf(fp,"%d\n",ParticleType[i]);}
 	fprintf(fp,"</DataArray>\n");
 	fprintf(fp,"<DataArray NumberOfComponents='1' type='Float32' Name='Velocity' format='ascii'>\n");
	for(int i=0;i<NumberOfParticle;i++){
		double val=sqrt(Velocity[i*3]*Velocity[i*3]+Velocity[i*3+1]*Velocity[i*3+1]+Velocity[i*3+2]*Velocity[i*3+2]);
		fprintf(fp,"%f\n",(float)val);
	}
 	fprintf(fp,"</DataArray>\n");
 	fprintf(fp,"<DataArray NumberOfComponents='1' type='Float32' Name='dens' format='ascii'>\n");
	for(int i=0;i<NumberOfParticle;i++){	fprintf(fp,"%f\n",(float)dens[i]);}
 	fprintf(fp,"</DataArray>\n");
  	fprintf(fp,"</PointData>\n");

 	fprintf(fp,"<Cells>\n");
 	fprintf(fp,"<DataArray type='Int32' Name='connectivity' format='ascii'>\n");
	for(int i=0;i<NumberOfParticle;i++)fprintf(fp,"%d\n",i);
 	fprintf(fp,"</DataArray>\n");
 	fprintf(fp,"<DataArray type='Int32' Name='offsets' format='ascii'>\n");
	for(int i=0;i<NumberOfParticle;i++)fprintf(fp,"%d\n",i+1);
 	fprintf(fp,"</DataArray>\n");
 	fprintf(fp,"<DataArray type='UInt8' Name='types' format='ascii'>\n");
	for(int i=0;i<NumberOfParticle;i++)fprintf(fp,"1\n");
 	fprintf(fp,"</DataArray>\n");
 	fprintf(fp,"</Cells>\n");

 	fprintf(fp,"</Piece>\n");
 	fprintf(fp,"</UnstructuredGrid>\n");
 	fprintf(fp,"</VTKFile>\n");

	fclose(fp);
	printf("done.\n");
}

int main( int argc, char** argv) {

	for(int iFile=0;iFile<FileNumber;iFile++){

		read_data(argv[1],iFile*10);
		mk_vtu(argv[1],iFile*10);

		free(Position);
		free(Velocity);
		free(Pressure);
		free(dens);
        free(eng);
		free(ParticleType);
	}
	printf(" END. \n");

	return 0;
}

