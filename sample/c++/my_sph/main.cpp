// Include FDPS header
#include <particle_simulator.hpp>
// Include the standard C++ headers
#include <cmath>
#include <cstdio>
#include <iostream>
#include <vector>
#include <sys/stat.h>


/* Class Definitions */
//** Force Class (Result Class)
class Pressure{
   public:
   PS::F64 pres;
   void clear(){
      pres = 0;
   }
};
class Viscosity{
   public:
   PS::F64 visc;
   void clear(){
      visc = 0;
   }
};
class Gravity{
    public:
    PS::F64vec grav;
}

//** Full Particle Class
struct FP{
    PS::F64 mass;
   PS::F64vec pos;
   PS::F64vec vel;
   PS::F64vec acc;
   PS::F64 dens;
   PS::F64 pres;
   PS::F64 visc;
   PS::F64vec grav;
   PS::S64 id;
   PS::S32 particle_type;
   void copyFromForce(const Pressure& pres){
      this->pres = pres.pres;
   }
   void copyFromForce(const Viscosity& visc){
      this->visc = visc.visc;
   }
   void copyFromForce(const Gravity& grav){
      this->grav = grav.grav;
   }
   PS::F64 getCharge() const{
      return this->mass;
   }
   PS::F64vec getPos() const{
      return this->pos;
   }
   PS::F64 getRSearch() const{
      return kernelSupportRadius * this->smth;
   }
   void setPos(const PS::F64vec& pos){
      this->pos = pos;
   }
   void writeAscii(FILE* fp) const{
      fprintf(fp,
              "%d\t%lf\t%lf\t%lf\t%lf\t%lf\t"
              "%lf\t%lf\t%lf\t%lf\t%lf\n",
              this->particle_type, this->mass,
              this->pos.x, this->pos.y, this->pos.z,
              this->vel.x, this->vel.y, this->vel.z,
              this->dens, this->eng, this->pres);
   }
   void readAscii(FILE* fp){
      fscanf(fp,
             "%d\t%lf\t%lf\t%lf\t%lf\t%lf\t"
             "%lf\t%lf\t%lf\t%lf\t%lf\n",
             &this->particle_type, &this->mass,
             &this->pos.x, &this->pos.y, &this->pos.z,
             &this->vel.x, &this->vel.y, &this->vel.z,
             &this->dens, &this->eng, &this->pres);
   }
   void setPressure(){
      const PS::F64 hcr = 1.4;
      pres = (hcr - 1.0) * dens * eng;
      snds = sqrt(hcr * pres / dens);
   }
};

int main(int argc, char* argv[]){
    // Initialize FDPS
    PS::Initialize(argc, argv);
    PS::Comm::barrier();
    printf("%d/%d\n", PS::Comm::getRank(), PS::Comm::getNumberOfProc());
    PS::Finalize();
}