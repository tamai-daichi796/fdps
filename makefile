all: a.out

CXX=g++
CXXFLAGS= -std=c++17

FDPS_PATH=$(HOME)/Documents/FDPS/FDPS/src
CPPFLAGS=-I$(FDPS_PATH)

a.out: test.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $< -o $@

clean:
	rm -f a.out